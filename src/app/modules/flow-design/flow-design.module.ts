import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FlowDesignRoutingModule } from './flow-design-routing.module';
import { LayoutComponent } from './layout/layout.component';
import { DefaultSideBarComponent } from './components/default-side-bar/default-side-bar.component';
import { FlowChartComponent } from './components/flow-chart/flow-chart.component';
import { FlowServiceService } from 'src/app/flow-service.service';
import { FlowChartV2Component } from './components/flow-chart-v2/flow-chart-v2.component';
import { FlowChartV3Component } from './components/flow-chart-v3/flow-chart-v3.component';


@NgModule({
  declarations: [
    LayoutComponent,
    DefaultSideBarComponent,
    FlowChartComponent,
    FlowChartV2Component,
    FlowChartV3Component
  ],
  imports: [
    CommonModule,
    FlowDesignRoutingModule
  ],
  providers: [
    FlowServiceService
  ]
})
export class FlowDesignModule { }
