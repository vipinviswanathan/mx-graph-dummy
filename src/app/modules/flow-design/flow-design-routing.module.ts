import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout/layout.component';
import { FlowChartComponent } from './components/flow-chart/flow-chart.component';
import { FlowChartV2Component } from './components/flow-chart-v2/flow-chart-v2.component';
import { FlowChartV3Component } from './components/flow-chart-v3/flow-chart-v3.component';


const routes: Routes = [
  {
    path: "",
    component: LayoutComponent,
    children: [
      {
        path: "",
        component: FlowChartComponent
      },
      {
        path: "v2",
        component: FlowChartV2Component
      },
      {
        path: "v3",
        component: FlowChartV3Component
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FlowDesignRoutingModule { }
