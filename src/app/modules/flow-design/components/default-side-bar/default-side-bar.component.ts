import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FlowServiceService } from 'src/app/flow-service.service';
import { ActivatedRoute } from '@angular/router'
declare var mxToolbar;
declare var mxClient;
declare var mxDivResizer;
declare var mxGeometry;
declare var mxCell;
declare var mxUtils;
declare var mxGraphModel;
declare var mxDragSource;
declare var mxKeyHandler;

@Component({
  selector: 'app-default-side-bar',
  templateUrl: './default-side-bar.component.html',
  styleUrls: ['./default-side-bar.component.scss']
})
export class DefaultSideBarComponent implements OnInit {
  @ViewChild('toolbar', { static: true }) toolbarContainer: ElementRef;

  stageList;
  toolbar;


  constructor(public flowServiceService: FlowServiceService, private route: ActivatedRoute) { }

  ngOnInit() {
    console.log(this.flowServiceService.graph);
    console.log(this.toolbarContainer.nativeElement);
    this.flowServiceService.graphInit.subscribe(
      event => {
        if (this.toolbar == null) {
          this.intiateToolbar();
        }
      }
    );

    if (mxClient.IS_QUIRKS) {
      new mxDivResizer(this.toolbarContainer.nativeElement);
    }
  }

  intiateToolbar() {
    // Creates new toolbar without event processing
    this.toolbar = new mxToolbar(this.toolbarContainer.nativeElement);
    this.toolbar.enabled = false

    // Workaround for Internet Explorer ignoring certain styles
    new mxDivResizer(this.toolbarContainer.nativeElement);
    if (mxClient.IS_QUIRKS) {
      document.body.style.overflow = 'auto';
    }

    let addVertex = (icon, w, h, style) => {
      let vertex = new mxCell(null, new mxGeometry(200, 0, w, h), style);
      vertex.setVertex(true);
      this.addToolbarItem(this.flowServiceService.graph, this.toolbar, vertex);
    };

    addVertex('assets/images/rectangle.gif', 100, 40, 'backgroundColor=red');
  }

  public addToolbarItem(graph, toolbar, prototype) {
    // Function that is executed when the image is dropped on
    // the graph. The cell argument points to the cell under
    // the mousepointer if there is one.
    let onDrag = function (graph, evt, cell) {
      graph.stopEditing(false);

      let pt = graph.getPointForEvent(evt);
      let vertex = graph.getModel().cloneCell(prototype);
      vertex.geometry.x = pt.x;
      vertex.geometry.y = pt.y;

      graph.setSelectionCells(graph.importCells([vertex], 0, 0, cell));
    }

    // Creates the image which is used as the drag icon (preview)
    // var img = toolbar.addMode(null, image, funct);
    let img = toolbar.addItem('stage', null, null, '', 'item_tool', (menu, evt, cell) => {
    });
    mxUtils.makeDraggable(img, graph, onDrag);
  }
}

