import { Component, OnInit, ViewChild, ElementRef, ViewEncapsulation, AfterViewInit, Inject, PLATFORM_ID } from '@angular/core';
import { FlowServiceService } from 'src/app/flow-service.service';
import { isPlatformBrowser } from '@angular/common';

declare var mxClient;
declare var mxUtils;
declare var mxConstants;
declare var mxEvent;
declare var mxGeometry;
declare var mxCell;
declare var mxKeyHandler;
declare var mxConnectionHandler;
declare var mxToolbar;
declare var mxDivResizer;
declare var mxGraphModel;
declare var mxGraph;
declare var mxDragSource;

@Component({
  selector: 'app-flow-chart',
  templateUrl: './flow-chart.component.html',
  styleUrls: ['./flow-chart.component.scss'],
  //   encapsulation: ViewEncapsulation.None
})
export class FlowChartComponent implements OnInit, AfterViewInit {
  @ViewChild('graphContainer', { static: true }) graphContainer: ElementRef;

  newGraph: mxGraph;

  constructor(
    private flowServiceService: FlowServiceService,
    @Inject(PLATFORM_ID) private platformId: string
  ) { }

  public ngOnInit() {

    this.flowServiceService.graph = new mxGraph(this.graphContainer.nativeElement);
    this.flowServiceService.graphInit.next({});
  }
  public ngAfterViewInit() {
    // this.updateGraph();
    if (isPlatformBrowser(this.platformId)) {

      this.mainDraw();
    }
  }

  //   public updateGraph() {
  //     if (!mxClient.isBrowserSupported()) {
  //       // Displays an error message if the browser is not supported.
  //       mxUtils.error('Browser is not supported!', 200, false);
  //     }
  //     this.newGraph = new mxGraph(this.graphContainer.nativeElement);
  //     this.newGraph.gridSize = 30;
  //     this.newGraph.setAllowDanglingEdges(false);
  //     this.newGraph.setEdgeLabelsMovable(false);
  //     this.newGraph.setConnectable(true);

  //     const parent = this.newGraph.getDefaultParent();
  //     this.newGraph.getModel().beginUpdate();

  //     // Changes the style for match the markup
  //     // Creates the default style for vertices
  //     var style = this.newGraph.getStylesheet().getDefaultVertexStyle();
  //     style[mxConstants.STYLE_STROKECOLOR] = 'gray';
  //     style[mxConstants.STYLE_ROUNDED] = true;
  //     style[mxConstants.STYLE_SHADOW] = true;
  //     style[mxConstants.STYLE_FILLCOLOR] = '#DFDFDF';
  //     style[mxConstants.STYLE_GRADIENTCOLOR] = 'white';
  //     style[mxConstants.STYLE_FONTCOLOR] = 'black';
  //     style[mxConstants.STYLE_FONTSIZE] = '12';
  //     style[mxConstants.STYLE_SPACING] = 4;

  //     // Creates the default style for edges
  //     style = this.newGraph.getStylesheet().getDefaultEdgeStyle();
  //     style[mxConstants.STYLE_STROKECOLOR] = '#0C0C0C';
  //     style[mxConstants.STYLE_LABEL_BACKGROUNDCOLOR] = 'white';
  //     style[mxConstants.STYLE_LABEL_BORDERCOLOR] = 'black';
  //     style[mxConstants.STYLE_LABEL_PADDING] = '30px !important';
  //     style[mxConstants.STYLE_EDGE] = mxEdgeStyle.ElbowConnector;
  //     style[mxConstants.STYLE_ROUNDED] = true;
  //     style[mxConstants.STYLE_FONTCOLOR] = 'black';
  //     style[mxConstants.STYLE_FONTSIZE] = '10';

  //     // Implements a properties panel that uses
  //     // mxCellAttributeChange to change properties
  //     this.newGraph.getSelectionModel().addListener(mxEvent.CHANGE, (sender, evt) => {
  //       this.selectionChanged(this.newGraph);
  //     });

  //     try {
  //       const vertex1 = this.newGraph.insertVertex(parent, '1', 'Vertex 1', 0, 0, 200, 80, 'fillColor=red', false);
  //       const vertex2 = this.newGraph.insertVertex(parent, '2', 'Vertex 2', 0, 0, 200, 80, 'fillColor=red', false);
  //       const vertex3 = this.newGraph.insertVertex(parent, '3', 'Vertex 3', 0, 0, 200, 80, 'fillColor=green', false);
  //       const vertex4 = this.newGraph.insertVertex(parent, '4', 'Vertex 4', 0, 0, 200, 80, 'fillColor=pink', false);
  //       this.newGraph.insertEdge(parent, null, 'my name', vertex1, vertex2, 'fillColor=red');
  //       this.newGraph.insertEdge(parent, null, 'my name 2', vertex1, vertex3, 'fillColor=green');
  //       this.newGraph.insertEdge(parent, null, 'my name 3', vertex2, vertex4, 'fillColor=green');
  //       this.newGraph.insertEdge(parent, null, 'my name 4', vertex3, vertex4, 'fillColor=green');



  //     } finally {
  //       this.newGraph.getModel().endUpdate();
  //       new mxHierarchicalLayout(this.newGraph).execute(this.newGraph.getDefaultParent());
  //     }
  //   }

  public selectionChanged(event: mxGraph) {
    console.log(event);
  }

  //   drop(event) {
  //     console.log(event);
  //   }



  // Program starts here. Creates a sample graph in the
  // DOM node with the specified ID. This function is invoked
  // from the onLoad event handler of the document (see below).
  public mainDraw() {
    // Defines an icon for creating new connections in the connection handler.
    // This will automatically disable the highlighting of the source vertex.
    mxConnectionHandler.prototype.connectImage = new mxImage('assets/images/connector.gif', 16, 16);

    // Checks if browser is supported
    if (!mxClient.isBrowserSupported()) {
      // Displays an error message if the browser is
      // not supported.
      mxUtils.error('Browser is not supported!', 200, false);
    }
    else {
      // Creates the div for the toolbar
      var tbContainer = document.createElement('div');
      tbContainer.style.position = 'relative';
      tbContainer.style.overflow = 'hidden';
      tbContainer.style.padding = '2px';
      tbContainer.style.left = '0px';
      tbContainer.style.top = '26px';
      tbContainer.style.width = '24px';
      tbContainer.style.bottom = '0px';

      document.body.appendChild(tbContainer);

      // Creates new toolbar without event processing
      var toolbar = new mxToolbar(tbContainer);
      toolbar.enabled = false

      // Workaround for Internet Explorer ignoring certain styles
      if (mxClient.IS_QUIRKS) {
        document.body.style.overflow = 'hidden';
        new mxDivResizer(tbContainer);
        new mxDivResizer(this.graphContainer.nativeElement);
      }

      // Creates the model and the graph inside the container
      // using the fastest rendering available on the browser
      let model = new mxGraphModel();
      var graph = new mxGraph(this.graphContainer.nativeElement, model);
      graph.dropEnabled = true;

      // Matches DnD inside the graph
      mxDragSource.prototype.getDropTarget = function (graph, x, y) {
        var cell = graph.getCellAt(x, y);
        console.log(cell);

        if (!graph.isValidDropTarget(cell)) {
          cell = null;
        }

        return cell;
      };

      // Enables new connections in the graph
      graph.setConnectable(true);
      graph.setMultigraph(false);



      // Stops editing on enter or escape keypress
      var keyHandler = new mxKeyHandler(graph);
      var rubberband = new mxRubberband(graph);

      var addVertex = (icon, w, h, style) => {
        var vertex = new mxCell(null, new mxGeometry(0, 0, w, h), style);
        vertex.setVertex(true);

        // // Creates a DOM node that acts as the drag source
        // var img = mxUtils.createImage('assets/images/rectangle.gif');
        // img.style.width = '48px';
        // img.style.height = '48px';
        // document.body.appendChild(img);

        var dragElt = document.createElement('div');
        dragElt.style.border = 'dashed black 1px';
        dragElt.style.width = '120px';
        dragElt.style.height = '40px';

        this.addToolbarItem(graph, toolbar, vertex, dragElt);
        // this.addToolbarItem(graph, toolbar, vertex, icon);
      };

      //   addVertex('assets/images/swimlane.gif', 120, 160, 'shape=swimlane;startSize=20;');
      addVertex('assets/images/rectangle.gif', 100, 40, 'backgroundColor=red');
      //   addVertex('assets/images/rounded.gif', 100, 40, 'shape=rounded');
      //   addVertex('assets/images/ellipse.gif', 40, 40, 'shape=ellipse');
      //   addVertex('assets/images/rhombus.gif', 40, 40, 'shape=rhombus');
      //   addVertex('assets/images/triangle.gif', 40, 40, 'shape=triangle');
      //   addVertex('assets/images/cylinder.gif', 40, 40, 'shape=cylinder');
      //   addVertex('assets/images/actor.gif', 30, 40, 'shape=actor');
      //   toolbar.addLine();
    }

    graph.gridSize = 30;
    graph.setAllowDanglingEdges(false);
    graph.setEdgeLabelsMovable(false);
    graph.setConnectable(true);

    const parent = graph.getDefaultParent();
    graph.getModel().beginUpdate();

    // Changes the style for match the markup
    // Creates the default style for vertices
    var style = graph.getStylesheet().getDefaultVertexStyle();
    style[mxConstants.STYLE_STROKECOLOR] = 'gray';
    style[mxConstants.STYLE_ROUNDED] = true;
    style[mxConstants.STYLE_SHADOW] = true;
    style[mxConstants.STYLE_FILLCOLOR] = '#DFDFDF';
    style[mxConstants.STYLE_GRADIENTCOLOR] = 'white';
    style[mxConstants.STYLE_FONTCOLOR] = 'black';
    style[mxConstants.STYLE_FONTSIZE] = '12';
    style[mxConstants.STYLE_SPACING] = 4;

    // Creates the default style for edges
    style = graph.getStylesheet().getDefaultEdgeStyle();
    style[mxConstants.STYLE_STROKECOLOR] = '#0C0C0C';
    style[mxConstants.STYLE_LABEL_BACKGROUNDCOLOR] = 'white';
    style[mxConstants.STYLE_LABEL_BORDERCOLOR] = 'black';
    style[mxConstants.STYLE_LABEL_PADDING] = '30px !important';
    style[mxConstants.STYLE_EDGE] = mxEdgeStyle.ElbowConnector;
    style[mxConstants.STYLE_ROUNDED] = true;
    style[mxConstants.STYLE_FONTCOLOR] = 'black';
    style[mxConstants.STYLE_FONTSIZE] = '10';

    var v1 = graph.insertVertex(parent, null, 'Hello,', 20, 20, 80, 30);
    var v2 = graph.insertVertex(parent, null, 'World!', 200, 150, 80, 30);
    var e1 = graph.insertEdge(parent, null, 'nothing', v1, v2);

    graph.getSelectionModel().addListener(mxEvent.CHANGE, (sender, evt) => {
      this.selectionChanged(graph);
    });
    graph.getModel().endUpdate();
  }

  public addToolbarItem(graph, toolbar, prototype, image) {
    console.log("here");
    // Function that is executed when the image is dropped on
    // the graph. The cell argument points to the cell under
    // the mousepointer if there is one.
    var funct = function (graph, evt, cell) {
      console.log(graph, evt, cell);
      graph.stopEditing(false);

      var pt = graph.getPointForEvent(evt);
      var vertex = graph.getModel().cloneCell(prototype);
      vertex.geometry.x = pt.x;
      vertex.geometry.y = pt.y;

      graph.setSelectionCells(graph.importCells([vertex], 0, 0, cell));
    }

    // Creates the image which is used as the drag icon (preview)
    // var img = toolbar.addMode(null, image, funct);
    var img = toolbar.addItem('stage', null, null, '', 'item_tool', (menu, evt, cell) => {
      // menu.addItem('Hello, World!');
      console.log(menu, evt, cell);
    });
    console.log(img);
    mxUtils.makeDraggable(img, graph, funct);
    // mxUtils.makeDraggable(img, funct);
  }
}

