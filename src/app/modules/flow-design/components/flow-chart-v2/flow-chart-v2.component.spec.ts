import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlowChartV2Component } from './flow-chart-v2.component';

describe('FlowChartV2Component', () => {
  let component: FlowChartV2Component;
  let fixture: ComponentFixture<FlowChartV2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlowChartV2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlowChartV2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
