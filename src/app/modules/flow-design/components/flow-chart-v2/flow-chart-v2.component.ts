import { Component, OnInit, ViewChild, ElementRef, Inject, PLATFORM_ID } from '@angular/core';
import { FlowServiceService } from 'src/app/flow-service.service';
declare var mxClient;
declare var mxUtils;
declare var mxConstants;
declare var mxEvent;
declare var mxCell;
declare var mxKeyHandler;
declare var mxConnectionHandler;
declare var mxDivResizer;
declare var mxGraphModel;
declare var mxGraph;
declare var mxDragSource;
declare var mxGraphHandler;
declare var mxParallelEdgeLayout;

@Component({
  selector: 'app-flow-chart-v2',
  templateUrl: './flow-chart-v2.component.html',
  styleUrls: ['./flow-chart-v2.component.scss']
})
export class FlowChartV2Component implements OnInit {

  @ViewChild('graphContainer', { static: true }) graphContainer: ElementRef;

  public graph;

  constructor(
    private flowServiceService: FlowServiceService,
    @Inject(PLATFORM_ID) private platformId: string
  ) { }

  ngOnInit() {
    this.mainDraw();
    this.flowServiceService.graph = this.graph;
    this.flowServiceService.graphInit.next({});
  }

  public mainDraw() {
    // Defines an icon for creating new connections in the connection handler.
    // This will automatically disable the highlighting of the source vertex.
    mxConnectionHandler.prototype.connectImage = new mxImage('/assets/images/connector.gif', 16, 16);

    // Checks if browser is supported
    if (!mxClient.isBrowserSupported()) {
      // Displays an error message if the browser is
      // not supported.
      mxUtils.error('Browser is not supported!', 200, false);
    }
    else {


      var doc = mxUtils.createXmlDocument();

      var person1 = doc.createElement('Person');
      person1.setAttribute('firstName', 'Daffy');
      person1.setAttribute('lastName', 'Duck');
      person1.setAttribute('lastNameGHDFGDFGDFG', true);

      // Workaround for Internet Explorer ignoring certain styles
      if (mxClient.IS_QUIRKS) {
        document.body.style.overflow = 'hidden';
        var x = new mxDivResizer(this.graphContainer.nativeElement);
        console.log(x);
      }

      // Creates the model and the graph inside the container
      // using the fastest rendering available on the browser
      let model = new mxGraphModel();
      this.graph = new mxGraph(this.graphContainer.nativeElement, model);
      this.graph.dropEnabled = true;



      // Matches DnD inside the graph
      mxDragSource.prototype.getDropTarget = function (graph, x, y) {
        var cell = graph.getCellAt(x, y);
        console.log(cell);

        if (!graph.isValidDropTarget(cell)) {
          cell = null;
        }

        return cell;
      };

      // Enables new connections in the graph
      //   this.graph.setConnectable(true);
      this.graph.setMultigraph(false);

      // Stops editing on enter or escape keypress
      var keyHandler = new mxKeyHandler(this.graph);
      var rubberband = new mxRubberband(this.graph);
    }

    this.graph.gridSize = 30;
    this.graph.setAllowDanglingEdges(false);
    this.graph.setEdgeLabelsMovable(false);
    this.graph.setConnectable(true);
    this.graph.getSelectionModel().setSingleSelection(true);
    this.graph.setCellsDeletable(true);

    const parent = this.graph.getDefaultParent();
    this.graph.getModel().beginUpdate();

    var parallelEdges = new mxParallelEdgeLayout(this.graph);
    parallelEdges.spacing = 20;
    parallelEdges.execute(parent);

    var style = this.graph.getStylesheet();
    console.log(style);
    // Changes the style for match the markup
    // Creates the default style for vertices
    style = this.graph.getStylesheet().getDefaultVertexStyle();
    style[mxConstants.STYLE_STROKECOLOR] = 'gray';
    style[mxConstants.STYLE_ROUNDED] = true;
    style[mxConstants.STYLE_SHADOW] = true;
    style[mxConstants.STYLE_FILLCOLOR] = '#DFDFDF';
    style[mxConstants.STYLE_GRADIENTCOLOR] = 'white';
    style[mxConstants.STYLE_FONTCOLOR] = 'black';
    style[mxConstants.STYLE_FONTSIZE] = '12';
    style[mxConstants.STYLE_SPACING] = 4;

    // Creates the default style for edges
    style = this.graph.getStylesheet().getDefaultEdgeStyle();
    style[mxConstants.STYLE_STROKECOLOR] = '#585236';
    style[mxConstants.STYLE_LABEL_BACKGROUNDCOLOR] = 'violet';
    style[mxConstants.STYLE_LABEL_BORDERCOLOR] = 'none';
    style[mxConstants.STYLE_LABEL_PADDING] = '30px !important';
    style[mxConstants.STYLE_EDGE] = mxEdgeStyle.ElbowConnector;
    style[mxConstants.STYLE_ROUNDED] = true;
    style[mxConstants.STYLE_FONTCOLOR] = 'black';
    style[mxConstants.STYLE_FONTSIZE] = '16';
    style[mxConstants.STYLE_STROKEWIDTH] = '3';


    var v1 = this.graph.insertVertex(parent, null, 'Hello,', 20, 20, 80, 30, "fillColor=red");
    var v2 = this.graph.insertVertex(parent, null, 'World!', 200, 150, 80, 30);
    var v3 = this.graph.insertVertex(parent, null, 'new one', 400, 20, 80, 30);
    var v3 = this.graph.insertVertex(parent, null, person1, 600, 20, 80, 30);
    var e1 = this.graph.insertEdge(parent, null, 'nothing', v1, v2);
    var e2 = this.graph.insertEdge(parent, null, 'nothing 2', v3, v2);


    this.graph.addListener(mxEvent.DOUBLE_CLICK, (sender, evt) => {
      console.log(sender, evt);
      // this.graph.removeCells([sender.cells[0]]);
      if (evt.properties.cell) {
        // evt.properties.cell.setVisible(false);
        this.graph.removeCells([evt.properties.cell]);
      }
    });

    this.graph.addListener(mxEvent.CELLS_REMOVED, (sender, evt) => {
      console.log("REMOVED", sender, evt);
    });

    this.graph.getModel().endUpdate();

    this.graph.getSelectionModel().addListener(mxEvent.CHANGE, (sender, evt) => {
      this.selectionChanged(sender, evt);
      if (sender.cells[0]) {
        var cell = sender.cells[0];
        var style = this.graph.getModel().getStyle(cell);
        var cs = new Array();
        if (sender.cells[0].isVertex()) {
          var newStyle = mxUtils.setStyle(style, mxConstants.STYLE_FILLCOLOR, 'green');
        } else if (sender.cells[0].isEdge()) {
          var newStyle = mxUtils.setStyle(style, mxConstants.STYLE_LABEL_BACKGROUNDCOLOR, 'orange');
        }
        cs[0] = cell;
        this.graph.setCellStyle(newStyle, cs);

      }
    });

    this.graph.addListener(mxEvent.CONNECT_CELL, (sender, evt) => {
      console.log("cell connect herer", sender, evt);
    });

    this.graph.connectionHandler.addListener(mxEvent.CONNECT, (sender, evt) => {
      // this.graph.getModel().beginUpdate();
      var edge = evt.getProperty('cell');
      edge.setValue("fgkjflgfg");
      var source = this.graph.getModel().getTerminal(edge, true);
      var target = this.graph.getModel().getTerminal(edge, false);
      console.log(edge, source, target, this.graph.getModel(), edge.cloneValue());

      // var style = this.graph.getCellStyle(edge);
      // var sourcePortId = style[mxConstants.STYLE_SOURCE_PORT];
      // var targetPortId = style[mxConstants.STYLE_TARGET_PORT];
      // this.graph.getModel().endUpdate();
      alert("new edge added");

    });
    // mxConnectionHandler.prototype.connect = function(source, target) {
    //   console.log("gggee,flsgfjksdnflsk");
    // };

    this.graph.connectionHandler.addListener(mxEvent.RESET, (sender, evt) => {
      console.log(sender, "im");
    });
  }

  public selectionChanged(sender, evt) {
    console.log(sender, evt);
    // alert();
  }

  submitGraph() {
    console.log(this.graph.getModel().cells)
  }
}
