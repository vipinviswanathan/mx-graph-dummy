import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlowChartV3Component } from './flow-chart-v3.component';

describe('FlowChartV3Component', () => {
  let component: FlowChartV3Component;
  let fixture: ComponentFixture<FlowChartV3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlowChartV3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlowChartV3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
