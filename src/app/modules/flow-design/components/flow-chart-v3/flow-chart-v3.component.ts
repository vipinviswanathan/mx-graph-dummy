import { Component, OnInit, ViewChild, ElementRef, Inject, PLATFORM_ID } from '@angular/core';
import { FlowServiceService } from 'src/app/flow-service.service';
declare var mxClient;
declare var mxUtils;
declare var mxConstants;
declare var mxEvent;
declare var mxEditor;
declare var mxGeometry;
declare var mxShape;
declare var mxKeyHandler;
declare var mxConnectionHandler;
declare var mxDivResizer;
declare var mxGraphModel;
declare var mxGraph;
declare var mxDragSource;
declare var mxOutline;
declare var mxCoordinateAssignment;
declare var mxParallelEdgeLayout;
declare var mxCellState;

@Component({
  selector: 'app-flow-chart-v3',
  templateUrl: './flow-chart-v3.component.html',
  styleUrls: ['./flow-chart-v3.component.scss']
})
export class FlowChartV3Component implements OnInit {

  @ViewChild('graphContainer', { static: true }) graphContainer: ElementRef;

  @ViewChild('outlineContainer', { static: true }) outlineContainer: ElementRef;

  public graph;

  constructor(
    private flowServiceService: FlowServiceService,
    @Inject(PLATFORM_ID) private platformId: string
  ) { }

  ngOnInit() {
    this.mainDraw();
    this.flowServiceService.graph = this.graph;
    this.flowServiceService.graphInit.next({});
  }

  public mainDraw() {
    // Defines an icon for creating new connections in the connection handler.
    // This will automatically disable the highlighting of the source vertex.
    mxConnectionHandler.prototype.connectImage = new mxImage('/assets/images/connector.gif', 16, 16);
    mxConnectionHandler.prototype.convertWaypoint = function (point) {
      console.log("point", point);
    }

    // Matches DnD inside the graph
    mxDragSource.prototype.getDropTarget = (graph, x, y) => {
      var cell = graph.getCellAt(x, y);

      if (!graph.isValidDropTarget(cell)) {
        cell = null;
      }

      return cell;
    };

    // Checks if browser is supported
    if (!mxClient.isBrowserSupported()) {
      // Displays an error message if the browser is
      // not supported.
      mxUtils.error('Browser is not supported!', 200, false);
    }

    // Workaround for Internet Explorer ignoring certain styles
    if (mxClient.IS_QUIRKS) {
      document.body.style.overflow = 'hidden';
      var x = new mxDivResizer(this.graphContainer.nativeElement);
      // console.log(x);
      // new mxDivResizer(this.outlineContainer.nativeElement);
    }

    // Creates the model and the graph inside the container
    // using the fastest rendering available on the browser
    let model = new mxGraphModel();
    // this.graph = new mxGraph(this.graphContainer.nativeElement);


    var editor = new mxEditor();
    editor.setGraphContainer(this.graphContainer.nativeElement);
    editor.readGraphModel(model);
    this.graph = editor.graph;
    this.graph.setConnectable(true);
    // console.log(editor, this.graph);

    editor.addListener(mxEvent.ADD_VERTEX, (sender, evt) => {
      console.log("event", sender, evt);
    });

    new mxHierarchicalLayout(this.graph);

    const parent = this.graph.getDefaultParent();
    this.graph.dropEnabled = false;
    this.graph.cellsEditable = false;
    this.graph.allowLoops = false; // self loops avoid
    this.graph.setCellsCloneable(false); // cntrl+drag cloning disable
    // Optional disabling of sizing
    this.graph.setCellsResizable(false);
    // Enables new connections in the graph
    this.graph.setConnectable(true);
    this.graph.setMultigraph(false);

    // Stops editing on enter or escape keypress
    var keyHandler = new mxKeyHandler(this.graph);
    // var rubberband = new mxRubberband(this.graph);

    // mxEvent.disableContextMenu(this.graphContainer.nativeElement);
    // Disables the built-in context menu
    mxEvent.disableContextMenu(this.graphContainer.nativeElement);

    this.graph.gridSize = 30;
    this.graph.setAllowDanglingEdges(false);
    this.graph.setEdgeLabelsMovable(false);
    this.graph.setConnectable(true);
    this.graph.getSelectionModel().setSingleSelection(true);
    this.graph.setAutoSizeCells(false);

    new mxOutline(this.graph, this.outlineContainer.nativeElement);
    this.graph.centerZoom = true;
    this.graph.setTooltips(!mxClient.IS_TOUCH);
    this.graph.SELECTION_DASHED = false;

    this.graph.setPanning(true);
    this.graph.centerZoom = true;
    this.graph.panningHandler.useLeftButtonForPanning = true;

    // new mxHierarchicalLayout(this.graph).execute(this.graph.getDefaultParent());
    new mxParallelEdgeLayout(this.graph).execute(this.graph.getDefaultParent());
    mxCoordinateAssignment.prototype.parallelEdgeSpacing = 30;  // test
    // var layout = new mxHierarchicalLayout(this.graph);
    // layout.execute(parent);

    console.log(this.graph);

    this.graph.getModel().beginUpdate();

    var style = this.graph.getStylesheet();
    // Changes the style for match the markup
    // Creates the default style for vertices
    style = this.graph.getStylesheet().getDefaultVertexStyle();
    style[mxConstants.STYLE_STROKECOLOR] = 'gray';
    style[mxConstants.STYLE_ROUNDED] = true;
    style[mxConstants.STYLE_SHADOW] = true;
    style[mxConstants.STYLE_FILLCOLOR] = '#DFDFDF';
    style[mxConstants.STYLE_GRADIENTCOLOR] = 'white';
    style[mxConstants.STYLE_FONTCOLOR] = 'black';
    style[mxConstants.STYLE_FONTSIZE] = '12';
    style[mxConstants.STYLE_SPACING] = 4;
    // style[mxConstants.STYLE_RESIZABLE] = 0;
    style[mxConstants.SELECTION_DASHED] = false;

    // Creates the default style for edges
    style = this.graph.getStylesheet().getDefaultEdgeStyle();
    style[mxConstants.STYLE_STROKECOLOR] = '#585236';
    style[mxConstants.STYLE_LABEL_BACKGROUNDCOLOR] = 'violet';
    style[mxConstants.STYLE_LABEL_BORDERCOLOR] = 'none';
    style[mxConstants.STYLE_LABEL_PADDING] = '30';
    // style[mxConstants.STYLE_EDGE] = mxEdgeStyle.ElbowConnector;
    style[mxConstants.STYLE_EDGE] = mxEdgeStyle.OrthConnector;
    // style[mxConstants.STYLE_EDGE] = "orthogonalEdgeStyle";
    style[mxConstants.STYLE_ROUNDED] = true;
    style[mxConstants.STYLE_FONTCOLOR] = 'black';
    style[mxConstants.STYLE_FONTSIZE] = '16';
    style[mxConstants.STYLE_STROKEWIDTH] = '3';

    // style = [];
		// 	style[mxConstants.STYLE_SHAPE] = mxConstants.SHAPE_ELLIPSE;
		// 	style[mxConstants.STYLE_FONTCOLOR] = 'gray';
		// 	style[mxConstants.STYLE_FILLCOLOR] = '#A0C88F';
		// 	style[mxConstants.STYLE_GRADIENTCOLOR] = 'white';
		// 	style[mxConstants.STYLE_STROKECOLOR] = '#A0C88F';
		// 	style[mxConstants.STYLE_ALIGN] = mxConstants.ALIGN_CENTER;
		// 	style[mxConstants.STYLE_VERTICAL_ALIGN] = mxConstants.ALIGN_MIDDLE;
		// 	style[mxConstants.STYLE_FONTSIZE] = 16;
		// 	this.graph.getStylesheet().putCellStyle('start', style);

    // var v1 = this.graph.insertVertex(null, 1, 'Hello,', 20, 20, 80, 30, "fillColor=red");
    // var v2 = this.graph.insertVertex(null, null, 'World!', 200, 150, 80, 30);
    // var v3 = this.graph.insertVertex(null, null, 'new one', 300, 20, 80, 30);
    // var v3 = this.graph.insertVertex(null, "null", person1, 600, 20, 80, 30, "fillColor=violet");
    // var e1 = this.graph.insertEdge(null, null, 'nothing', v1, v2);
    // var e2 = this.graph.insertEdge(null, null, 'nothing 2', v3, v2);
    this.flowServiceService.stagesV3.map((item, key) => {
      let doc = mxUtils.createXmlDocument();
      let data = doc.createElement('data');
      data.setAttribute('mockId', item.mockId);
      item['cell'] = this.graph.insertVertex(
        parent,
        item.mockId,
        data,
        item.geometry.dx,
        item.geometry.dy,
        item.geometry.width,
        item.geometry.height,
        "fillColor=" + item.color
      );
    });
    this.flowServiceService.transitionsV3.map((item, key) => {
      let doc = mxUtils.createXmlDocument();
      let data = doc.createElement('data');
      data.setAttribute('mockId', item.mockId);
      const sourceIndex = this.flowServiceService.stagesV3.findIndex(stage => stage.mockId == item.sourceId);
      const targetIndex = this.flowServiceService.stagesV3.findIndex(stage => stage.mockId == item.targetId);
      if (sourceIndex > -1 && targetIndex > -1) {
        item['cell'] = this.graph.insertEdge(
          parent,
          item.mockId,
          data,
          this.flowServiceService.stagesV3[sourceIndex].cell,
          this.flowServiceService.stagesV3[targetIndex].cell,
          mxConstants.STYLE_LABEL_BACKGROUNDCOLOR + '= ' + item.color
        );
        // item['cell'].geometry.points = [new mxPoint(item.point.x, item.point.y)];
        // item['cell'].geometry.points = [item.point];
        // item['cell'].geometry.points = item.points;
        item['cell'].geometry.points = item.point.filter(point => {
          return new mxPoint(point.x, point.y);
        });
        // item['cell'].geometry.cellBounds = item.bounds;
      }
    });

    console.log(this.flowServiceService.transitionsV3);
    // console.log(JSON.stringify(this.flowServiceService.transitionsV3));

    // method to override the label setting method for manual settings
    this.graph.convertValueToString = (cell) => {
      if (mxUtils.isNode(cell.value)) {
        if (cell.isEdge()) {
          const index = this.flowServiceService.transitionsV3.findIndex(item => item.mockId == cell.getAttribute('mockId', ''));
          if (index > -1) {
            return this.flowServiceService.transitionsV3[index].name;
          }
        } else {
          const index = this.flowServiceService.stagesV3.findIndex(item => item.mockId == cell.getAttribute('mockId', ''));
          if (index > -1) {
            return this.flowServiceService.stagesV3[index].name;
          }
        }
      }
    };

    this.graph.getModel().endUpdate();

    this.graph.addListener(mxEvent.DOUBLE_CLICK, (sender, evt) => {
      console.log(sender, evt);
      // this.graph.removeCells([sender.cells[0]]);
      if (evt.properties.cell) {
        // evt.properties.cell.setVisible(false);
        this.graph.removeCells([evt.properties.cell]);
      }
    });

    this.graph.addListener(mxEvent.CELLS_REMOVED, (sender, evt) => {
      console.log("REMOVED", sender, evt);
    });

    this.graph.getSelectionModel().addListener(mxEvent.CHANGE, (sender, evt) => {
      this.selectionChanged(sender, evt);
      if (sender.cells[0]) {
        var cell = sender.cells[0];
        var style = this.graph.getModel().getStyle(cell);
        var cs = new Array();
        if (sender.cells[0].isVertex()) {
          var newStyle = mxUtils.setStyle(style, mxConstants.STYLE_FILLCOLOR, 'green');
        } else if (sender.cells[0].isEdge()) {
          var newStyle = mxUtils.setStyle(style, mxConstants.STYLE_LABEL_BACKGROUNDCOLOR, 'orange');
        }
        cs[0] = cell;
        this.graph.setCellStyle(newStyle, cs);

      }
    });

    this.graph.addListener(mxEvent.CONNECT_CELL, (sender, evt) => {
      console.log("cell connect herer", sender, evt);
      alert("connection direction changed");
    });

    this.graph.addListener(mxEvent.CELLS_MOVED, (sender, evt) => {
      console.log("cell moved", sender, evt);
      alert("cell moved");
    });
    this.graph.addListener(mxEvent.CELLS_ADDED, (sender, evt) => {
      console.log("cell added herer", sender, evt);
      alert("cell added");
    });

    this.graph.view.addListener(mxEvent.SCALE, (sender, evt) => {
      console.log("cell resized herer", sender, evt);
      alert("cell resized");
    });


    this.graph.connectionHandler.addListener(mxEvent.CONNECT, (sender, evt) => {
      this.graph.getModel().beginUpdate();
      var edge = evt.getProperty('cell');
      let doc = mxUtils.createXmlDocument();
      let data = doc.createElement('data');
      data.setAttribute('mockId', "mock-3");
      edge.setValue(data);
      var source = this.graph.getModel().getTerminal(edge, true);
      var target = this.graph.getModel().getTerminal(edge, false);
      this.flowServiceService.transitionsV3.push({
        id: null,
        mockId: "mock-3",
        sourceId: source.getAttribute('mockId', ''),
        targetId: target.getAttribute('mockId', ''),
        name: "create",
        color: "orange"
      })
      // console.log(edge, source, target, this.graph.getModel(), edge.cloneValue());

      var style = this.graph.getCellStyle(edge);
      this.graph.getModel().endUpdate();
      // alert("new edge added");



    });
  }

  public selectionChanged(sender, evt) {
    if (sender.cells[0] && sender.cells[0].isEdge()) {
      console.log(this.graph, sender.cells[0]);
      let cellState = new mxCellState(this.graph.view, sender.cells[0]);
      console.log(cellState, this.graph.view);
      console.log(cellState.absoluteOffset);
      console.log(this.graph.view.states);
    }
    if (sender.cells[0] && sender.cells[0].isVertex()) {
      console.log(this.graph, sender.cells[0]);
    }
  }

  submitGraph() {
    console.log(this.graph.getModel().cells);
    console.log(this.flowServiceService.stagesV3, this.flowServiceService.transitionsV3);
  }
}
