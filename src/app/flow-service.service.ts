import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
declare var mxPoint;
declare var mxRectangle;

@Injectable()
export class FlowServiceService {
  public stagesV2: any[] = [
    {
      id: 1,
      start: true,
      initial: false,
      name: "Start",
      color: 'skyblue'
    },
    {
      id: 2,
      start: false,
      initial: true,
      name: "first",
      color: 'red'
    },
    {
      id: 3,
      start: false,
      initial: false,
      name: "second",
      color: 'green'
    }
  ];

  public transitionsV2: any[] = [
    {
      id: 0,
      sourceId: 1,
      targetId: 2,
      name: "create"
    },
    {
      id: 1,
      sourceId: 2,
      targetId: 3,
      name: "transition 1"
    }
  ];


  public stagesV3: any[] = [
    {
      id: 1,
      mockId: "jhfgs-1",
      start: true,
      initial: false,
      name: "Start",
      color: 'skyblue',
      geometry: {
        width: 80,
        height: 30,
        dx: 20,
        dy: 20
      }
    },
    {
      id: 2,
      mockId: "jhfgs-2",
      start: false,
      initial: true,
      name: "first",
      color: 'red',
      geometry: {
        width: 80,
        height: 30,
        dx: 200,
        dy: 150
      }
    },
    {
      id: 3,
      mockId: "jhfgs-3",
      start: false,
      initial: false,
      name: "second",
      color: 'green',
      geometry: {
        width: 80,
        height: 30,
        dx: 300,
        dy: 20
      }
    }
  ];

  public transitionsV3: any[] = [
    {
      id: 0,
      mockId: "hhd-1",
      sourceId: "jhfgs-1",
      targetId: "jhfgs-2",
      name: "create",
      color: "orange",
      point: [
        {x: 100, y: 35},
        {x: 150, y: 35},
        {x: 150, y: 360},
        {x: 240, y: 360},
        {x: 240, y: 180}
      ],
      points: [
        new mxPoint(100, 35),
        new mxPoint(150, 35),
        new mxPoint(150, 360),
        new mxPoint(240, 360),
        new mxPoint(240, 180)
      ],
      geo: {x: 280, y: 50, width: 20, height: 100},
      bounds: new mxRectangle (280, 35, 20, 115)
    },
    {
      id: 1,
      mockId: "hhd-2",
      sourceId: "jhfgs-2",
      targetId: "jhfgs-3",
      name: "transition 1",
      color: "gray",
      point: [
        {x: 280, y: 165},
        {x: 300, y: 165},
        {x: 300, y: 282},
        {x: 510, y: 282},
        {x: 510, y: 35},
        {x: 380, y: 35}
      ],
      points: [
        new mxPoint(280, 165),
        new mxPoint(300, 165),
        new mxPoint(300, 282),
        new mxPoint(510, 282),
        new mxPoint(510, 35),
        new mxPoint(380, 35)
      ],
      geo: {x: 280, y: 50, width: 20, height: 100},
      bounds: new mxRectangle (280, 35, 20, 115)

    }
  ];

  public graph: mxGraph = null;

  public graphInit = new Subject<{}>();

  constructor() { }
}
